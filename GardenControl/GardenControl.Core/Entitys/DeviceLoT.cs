﻿using GardenControl.Core.BaseEntity;

namespace GardenControl.Core.Entitys
{
    /// <summary>
    /// Класс, описывает данные об LoT устройстве
    /// </summary>
    public class DeviceAbstarctLoT : DeviceAbstarct
    {
        /// <summary>
        /// Список сенсоров, которые подключены к текущему устройству
        /// </summary>
        private List<Sensor> Sensors { get; set; }
    }
}
