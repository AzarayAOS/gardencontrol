﻿using GardenControl.Core.BaseEntity;

namespace GardenControl.Core.Entitys
{
    /// <summary>
    /// Данный класс описывает значение температуры, полученной с сенсора
    /// </summary>
    public class Temperature : SensorValueAbstract
    {
        /// <summary>
        /// Температура, полученное с сенсора устройства
        /// </summary>
        public double CurrentValue { get; set; }

    }
}
