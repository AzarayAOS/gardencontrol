﻿using System.ComponentModel.DataAnnotations;

namespace GardenControl.Core.BaseEntity
{
    /// <summary>
    /// Класс является базовым классом для всех устройств и датчиков, которые способны отправлять информацию
    /// </summary>
    public class BaseClassAbstract
    {
        /// <summary>
        /// идентификатор записи
        /// </summary>
        [Key]
        public long Id { get; set; }
    }
}
