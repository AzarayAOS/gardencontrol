﻿namespace GardenControl.Core.BaseEntity
{

    /// <summary>
    /// Класс является базовым для класса, в котором описываются данные с различных сенсоров
    /// </summary>
    public abstract class SensorValueAbstract : BaseClassAbstract
    {

        /// <summary>
        /// Идентификатор устройства, на котором присутствует сенсор
        /// </summary>
        public Guid DeviceId { get; set; }

        /// <summary>
        /// Идентификатор сенсора, с которого получено значение
        /// </summary>
        public Guid SensorGuid { get; set; }

        /// <summary>
        /// Дата получения значения 
        /// </summary>
        public DateTime DateTime { get; set; }
    }
}
