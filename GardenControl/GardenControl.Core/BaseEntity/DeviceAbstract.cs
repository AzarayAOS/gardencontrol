﻿namespace GardenControl.Core.BaseEntity;


/// <summary>
/// Класс описывает данные об устройсве, которое способно передавать данные
/// </summary>
public abstract class DeviceAbstarct : BaseClassAbstract
{
    /// <summary>
    /// Наименование устройства
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Отображаемое имя устройства
    /// </summary>
    public string DisplayName { get; set; }

}