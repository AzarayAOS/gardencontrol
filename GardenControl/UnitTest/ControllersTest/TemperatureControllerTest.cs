﻿using GardenControl.API.Controllers;

using GardenControl.API.Interfaces;
using GardenControl.Core.Entitys;

using Microsoft.AspNetCore.Mvc;

using Moq;


namespace UnitTest.ControllersTest
{
    public class TemperatureControllerTest
    {
        [Fact]
        public async Task GetAllTemperatureAsyncTest()
        {

            // Arrange
            var testList = getTemperatures();
            var mock = new Mock<IRepository<Temperature>>();
            mock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(getTemperatures);
            TemperatureController controller = new TemperatureController(mock.Object);


            // Act
            var result = await controller.GetAllTemperatureAsync();
            var actual = (result.Result as OkObjectResult).Value as IEnumerable<Temperature>;
            // Assert
            IEnumerable<Temperature> vievResult = Assert.IsType<List<Temperature>>(actual);

            var model = Assert.IsAssignableFrom<IEnumerable<Temperature>>(actual);
            Assert.Equal(testList.Count, model.Count());


        }


        [Fact]
        public async Task GetTemperatureByDeviceIdAsyncTest()
        {
            // Arrange
            var testList = getTemperatures();
            var testGuid = testList[new Random().Next(testList.Count)].DeviceId;
            var testTemperature = testList
                .Where(x => x.DeviceId
                    .Equals(testGuid)).ToList();
            var mock = new Mock<IRepository<Temperature>>();
            mock.Setup(rep => rep.GetByDeviceIdAsync(testGuid)).ReturnsAsync(testTemperature);
            TemperatureController controller = new TemperatureController(mock.Object);


            // act
            var result = await controller.GetTemperatureByDeviceIdAsync(testGuid);
            var actual = (result.Result as OkObjectResult).Value as IEnumerable<Temperature>;

            // Assert
            IList<Temperature> vievResult = Assert.IsType<List<Temperature>>(actual);
            var model = Assert.IsAssignableFrom<IEnumerable<Temperature>>(actual);
            Assert.Equal(testTemperature.Count(), model.Count());
            Assert.True(testTemperature.SequenceEqual(model, new TemperatureComparer()));
        }


        [Fact]
        public async Task GetTemperatureByIdAsyncTest()
        {
            // Arrange
            var testList = getTemperatures();
            var testGuid = testList[new Random().Next(testList.Count)].Id;
            var testTemperature = testList
                .Where(x => x.Id.Equals(testGuid))
                .ToList()
                .FirstOrDefault();

            var mock = new Mock<IRepository<Temperature>>();
            mock
                .Setup(rep => rep.GetByIdAsync(testGuid))!
                .ReturnsAsync(testTemperature);
            TemperatureController controller = new TemperatureController(mock.Object);

            //act
            var result = await controller.GetTemperatureByIdAsync(testGuid);
            var actual = (result.Result as OkObjectResult).Value as Temperature;

            //Assert
            var viewResult = Assert.IsType<Temperature>(actual);
            var model = Assert.IsAssignableFrom<Temperature>(actual);
            Assert.True(new TemperatureComparer().Equals(testTemperature, model));
        }


        [Fact]
        public async Task GetTemperatureBySensorIdAsyncTest()
        {
            // Arrange
            var testList = getTemperatures();
            var testGuid = testList[new Random().Next(testList.Count)].SensorGuid;
            var testTemperature = testList
                .Where(x => x.SensorGuid
                    .Equals(testGuid)).ToList();
            var mock = new Mock<IRepository<Temperature>>();
            mock.Setup(rep => rep.GetByDeviceIdAsync(testGuid)).ReturnsAsync(testTemperature);
            TemperatureController controller = new TemperatureController(mock.Object);


            // act
            var result = await controller.GetTemperatureByDeviceIdAsync(testGuid);
            var actual = (result.Result as OkObjectResult).Value as IEnumerable<Temperature>;

            // Assert
            IList<Temperature> vievResult = Assert.IsType<List<Temperature>>(actual);
            var model = Assert.IsAssignableFrom<IEnumerable<Temperature>>(actual);
            Assert.Equal(testTemperature.Count(), model.Count());
            Assert.True(testTemperature.SequenceEqual(model, new TemperatureComparer()));
        }

        private List<Temperature> getTemperatures()
        {
            return new List<Temperature>()
            {
                new Temperature
                {
                    Id = 1U, DeviceId = Guid.NewGuid(), SensorGuid = Guid.NewGuid(),
                    DateTime = DateTime.Now.AddHours(1).ToUniversalTime(), CurrentValue = 15
                },
                new Temperature
                {
                    Id = 2U, DeviceId = Guid.NewGuid(), SensorGuid = Guid.NewGuid(),
                    DateTime = DateTime.Now.AddHours(2).ToUniversalTime(), CurrentValue = 16
                },
                new Temperature
                {
                    Id = 3U, DeviceId = Guid.NewGuid(), SensorGuid = Guid.NewGuid(),
                    DateTime = DateTime.Now.AddHours(3).ToUniversalTime(), CurrentValue = 17
                }
            };
        }

        private class TemperatureComparer : IEqualityComparer<Temperature>
        {
            public bool Equals(Temperature x, Temperature y)
            {
                if (Object.ReferenceEquals(x, y)) return true;

                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                return x.CurrentValue == y.CurrentValue &&
                       x.DeviceId.Equals(y.DeviceId) &&
                       x.DateTime.Equals(y.DateTime) &&
                       x.Id == y.Id;
            }


            public int GetHashCode(Temperature x)
            {
                if (Object.ReferenceEquals(x, null)) return 0;

                int hashID = x.Id.GetHashCode();
                int hashDateTime = x.DateTime.GetHashCode();
                int hashDeviceID = x.DeviceId.GetHashCode();
                double hashCurrentValue = x.CurrentValue;

                return hashID ^ hashDateTime ^ hashDeviceID ^ Convert.ToInt32(Math.Round(hashCurrentValue, MidpointRounding.AwayFromZero));

            }

        }
    }
}
