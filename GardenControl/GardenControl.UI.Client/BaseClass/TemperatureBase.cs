﻿using GardenControl.Core.Entitys;
using GardenControl.UI.Client.Services.Contracts;

using Microsoft.AspNetCore.Components;

namespace GardenControl.UI.Client.BaseClass
{
    public class TemperatureBase : ComponentBase
    {
        [Inject]
        public ITemperatureService TemperatureService { get; set; }

        public IEnumerable<Temperature> TemperatureList { get; set; }

        public IEnumerable<Temperature> TemperatureSensorList { get; set; }


        protected override async Task OnInitializedAsync()
        {
            TemperatureList = await TemperatureService.GetItems();
            TemperatureSensorList = await TemperatureService.GetBySensorId(Guid.Parse("704a39f0-b090-4101-a6f8-6d5b12bc4fd7"));
        }

        public async Task<IEnumerable<Temperature>> GetBySensorIdAsync(Guid sensorGuid)
        {
            return await TemperatureService.GetBySensorId(sensorGuid);
        }
    }
}
