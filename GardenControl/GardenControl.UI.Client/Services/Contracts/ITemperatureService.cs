﻿using GardenControl.Core.Entitys;

namespace GardenControl.UI.Client.Services.Contracts;

public interface ITemperatureService
{
    /// <summary>
    /// Получить все записи температуры
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<Temperature>> GetItems();

    /// <summary>
    /// Получить все значения температуры с конкретного датчика, по его идентификатору
    /// </summary>
    /// <param name="sensorGuid"></param>
    /// <returns></returns>
    Task<IEnumerable<Temperature>> GetBySensorId(Guid sensorGuid);

}