﻿using GardenControl.Core.Entitys;
using GardenControl.UI.Client.Services.Contracts;

using System.Net.Http.Json;

namespace GardenControl.UI.Client.Services
{
    public class TemperatureService : ITemperatureService
    {
        private readonly HttpClient _httpClient;
        private readonly string APIConect = "/api/v1/Temperature/";
        public TemperatureService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }


        public async Task<IEnumerable<Temperature>> GetItems()
        {
            try
            {
                var TemperatureList = await _httpClient.GetFromJsonAsync<IEnumerable<Temperature>>(APIConect);

                return TemperatureList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }

        public async Task<IEnumerable<Temperature>> GetBySensorId(Guid sensorGuid)
        {
            try
            {
                var sensorListGuid =
                    await _httpClient.GetFromJsonAsync<IEnumerable<Temperature>>($"{APIConect}SensorId={sensorGuid}");

                List<Temperature> tempTemperature = new List<Temperature>(sensorListGuid.Count());

                foreach (var temperature in sensorListGuid)
                {
                    Temperature temp = new Temperature();
                    temp.CurrentValue = temperature.CurrentValue;
                    temp.DateTime = temperature.DateTime;
                    temp.DeviceId = temperature.DeviceId;
                    temp.SensorGuid = temperature.SensorGuid;
                    temp.Id = temperature.Id;

                    tempTemperature.Add(temp);
                }

                return tempTemperature;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
