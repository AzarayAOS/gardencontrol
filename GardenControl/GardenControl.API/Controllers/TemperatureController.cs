﻿
using GardenControl.API.Interfaces;
using GardenControl.Core.Entitys;

using Microsoft.AspNetCore.Mvc;

namespace GardenControl.API.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/v1/[controller]")]
    public class TemperatureController : ControllerBase
    {

        private readonly IRepository<Temperature> _temperatuRepository;

        public TemperatureController(IRepository<Temperature> temperatuRepository)
        {
            _temperatuRepository = temperatuRepository;
        }

        /// <summary>
        /// Получить все записи в таблице температур
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Temperature>>> GetAllTemperatureAsync()
        {
            try
            {

                return Ok(await _temperatuRepository.GetAllAsync().ConfigureAwait(false));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest("Error Get All temperature");
            }

        }


        /// <summary>
        /// Получить все записи, сделанные с определённого устройства
        /// </summary>
        /// <param name="deviseId">GUID устройства</param>
        /// <returns></returns>
        [HttpGet("deviseId={deviseId:guid}")]
        public async Task<ActionResult<IEnumerable<Temperature>>> GetTemperatureByDeviceIdAsync(Guid deviseId)
        {
            try
            {
                return (Ok(await _temperatuRepository.GetByDeviceIdAsync(deviseId).ConfigureAwait(false)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest("Error Get Temp Device");
            }
        }

        /// <summary>
        /// Получить конкретную запись
        /// </summary>
        /// <param name="Id">Id записи</param>
        /// <returns></returns>
        [HttpGet("id={Id}")]
        public async Task<ActionResult<Temperature>> GetTemperatureByIdAsync(long Id)
        {
            try
            {
                return (Ok(await _temperatuRepository.GetByIdAsync(Id)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest("Error Get Temp Id");
            }
        }


        /// <summary>
        /// Получить все данные с конкретного датчика
        /// </summary>
        /// <param name="SensorId">GUID сенсора</param>
        /// <returns></returns>
        [HttpGet("SensorId={SensorId:guid}")]
        public async Task<IEnumerable<Temperature>> GetTemperatureBySensorIdAsync(Guid SensorId)
        {
            return await _temperatuRepository.GetBySensorIdAsync(SensorId);
        }

        /// <summary>
        /// Возвращает данные сенсора <see cref="SensorId"/> в диапазоне даты/время [<see cref="StartDateTime"/>:<see cref="StopDateTime"/>]
        /// </summary>
        /// <param name="SensorId">GUID сенсора</param>
        /// <param name="StartDateTime">Дата\время начало диапазона, включительно</param>
        /// <param name="StopDateTime">Дата\время конца диапазона, включительно</param>
        /// <returns></returns>
        [HttpGet("SensorId={SensorId:guid}&StartDateTime={StartDateTime:datetime}&StopDateTime={StopDateTime:datetime}")]
        public async Task<IEnumerable<Temperature>> GetBySensorIDAndDateAsync(Guid SensorId, DateTime StartDateTime,
            DateTime StopDateTime)
        {
            return await _temperatuRepository.GetBySensorIDAndDateAsync(SensorId, StartDateTime, StopDateTime);
        }

        /// <summary>
        /// Создание записи температуры
        /// </summary>
        /// <param name="temperatureCrate">Объект записи температуры, который надо добавить в таблицу</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateTemperatureAsync(Temperature temperatureCrate)
        {
            if (temperatureCrate is null) return BadRequest();

            Temperature temperature = new Temperature();
            temperature.CurrentValue = temperatureCrate.CurrentValue;
            temperature.DateTime = temperatureCrate.DateTime;
            temperature.DeviceId = temperatureCrate.DeviceId;
            //temperature.Id = temperatureCrate.Id;
            temperature.SensorGuid = temperatureCrate.SensorGuid;

            var own = await _temperatuRepository.CreateAsync(temperature);

            return Ok();

        }



        /// <summary>
        /// Обновление существующей записи
        /// </summary>
        /// <param name="temperaturePut">Объект записи температуры, который надо обновить</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateAsync(Temperature temperaturePut)
        {
            if (temperaturePut is null) return BadRequest();

            Temperature temperature = new Temperature();
            temperature.CurrentValue = temperaturePut.CurrentValue;
            temperature.DateTime = temperaturePut.DateTime;
            temperature.DeviceId = temperaturePut.DeviceId;
            temperature.Id = temperaturePut.Id;
            temperature.SensorGuid = temperaturePut.SensorGuid;


            var own = await _temperatuRepository.UpdateAsync(temperature);

            return Ok();
        }



        /// <summary>
        /// Удалить значение в таблице
        /// </summary>
        /// <param name="temperatureDelete">Объект записи температуры, который надо удалить</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(Temperature temperatureDelete)
        {
            if (temperatureDelete is null) return BadRequest();

            var temperature = new Temperature();
            temperature.CurrentValue = temperatureDelete.CurrentValue;
            temperature.DateTime = temperatureDelete.DateTime;
            temperature.DeviceId = temperatureDelete.DeviceId;
            temperature.Id = temperatureDelete.Id;
            temperature.SensorGuid = temperatureDelete.SensorGuid;


            var own = await _temperatuRepository.DeleteAsync(temperature);

            return Ok();
        }


    }
}
