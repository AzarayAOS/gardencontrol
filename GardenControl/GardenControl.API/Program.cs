using GardenControl.API.Contexts;
using GardenControl.API.Interfaces;
using GardenControl.API.Repositorys;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;

using Serilog;

using System.Reflection;

var configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build();

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration)
    .WriteTo.Console()
    .CreateBootstrapLogger();

Log.Information("Starting up");


try
{
    var builder = WebApplication.CreateBuilder(args);
    builder.Configuration
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", false, true)
        .AddEnvironmentVariables();

    var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
    // �������� ������� � ���������.

    builder.Services.AddControllers();
    builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepositoryTemperature<>));
    // ������� ������ � ��������� Swagger/OpenAPI �� ������ https://aka.ms/aspnetcore/swashbuckle.
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
    builder.Services.AddDbContext<DataContext>(options =>
    {
        options.UseLazyLoadingProxies().UseNpgsql(connectionString);
    });

    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo()
        {
            Title = "�������� ����� ��� ������� API",
            Version = "v1",
            Description = "REST, CORS, ...",
            Contact = new OpenApiContact()
            {
                Name = "�������� �����",
                Email = "AzarayAOS@mail.ru"
            }
        });

        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, Assembly.GetExecutingAssembly().GetName().Name + ".xml"));

    });

    builder.Services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

    builder.Host.UseSerilog((ctx, lockCookie) => lockCookie
        .ReadFrom.Configuration(configuration)
        .WriteTo.Console());
    builder.Services.AddDatabaseDeveloperPageExceptionFilter();

    builder.Services.AddSwaggerGen(c =>
{
    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
    c.IgnoreObsoleteActions();
    c.IgnoreObsoleteProperties();
    c.CustomSchemaIds(type => type.FullName);
});
    var app = builder.Build();

    // ��������� �������� HTTP-��������.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseCors(policy => policy.WithOrigins("http://localhost:7215", "https://localhost:7215")
        .AllowAnyOrigin()
        .WithHeaders((HeaderNames.ContentType))
        );


    app.UseSerilogRequestLogging();

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();



    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled exception");
}
finally
{
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}
