﻿
using GardenControl.Core.Entitys;

using Microsoft.EntityFrameworkCore;

namespace GardenControl.API.Contexts
{
    public class DataContext : DbContext
    {
        private IConfiguration _config;

        public DbSet<Temperature> TemperatureSets { get; set; }
        public DbSet<Sensor> Sensors { get; set; }
        public DbSet<DeviceAbstarctLoT> DeviceLoTs { get; set; }

        public DataContext(DbContextOptions<DataContext> options, IConfiguration config)
        : base(options)
        {

            _config = config;
            Database.EnsureCreated();

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Temperature>().HasData(
                new Temperature { Id = 1U, DeviceId = Guid.NewGuid(), SensorGuid = Guid.NewGuid(), DateTime = DateTime.Now.AddHours(1).ToUniversalTime(), CurrentValue = 15 },
                new Temperature { Id = 2U, DeviceId = Guid.NewGuid(), SensorGuid = Guid.NewGuid(), DateTime = DateTime.Now.AddHours(2).ToUniversalTime(), CurrentValue = 16 },
                new Temperature { Id = 3U, DeviceId = Guid.NewGuid(), SensorGuid = Guid.NewGuid(), DateTime = DateTime.Now.AddHours(3).ToUniversalTime(), CurrentValue = 17 }
                );

            modelBuilder.Entity<Sensor>().HasData(
               new Sensor { Id = 1U, Name = $"Sendor1_{DateTime.Now.Day}", DisplayName = "Sensor_1" },
               new Sensor { Id = 2U, Name = $"Sendor2_{DateTime.Now.Day + 1}", DisplayName = "Sensor_2" },
               new Sensor { Id = 3U, Name = $"Sendor3_{DateTime.Now.Day + 2}", DisplayName = "Sensor_3" }
               );

            modelBuilder.Entity<DeviceAbstarctLoT>().HasData(
                new DeviceAbstarctLoT { Id = 1U, Name = $"Device1_{DateTime.Now.Day}", DisplayName = "Device_1" },
                new DeviceAbstarctLoT { Id = 2U, Name = $"Device2_{DateTime.Now.Day + 1}", DisplayName = "Device_2" },
                new DeviceAbstarctLoT { Id = 3U, Name = $"Device2_{DateTime.Now.Day + 2}", DisplayName = "Device_2" }

                        );
        }

    }
}
