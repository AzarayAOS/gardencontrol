﻿namespace GardenControl.API.Interfaces
{
    public interface IRepository<T>
    {
        public Task<IEnumerable<T>> GetAllAsync();

        public Task<T> GetByIdAsync(long id);

        public Task<IEnumerable<T>> GetByDeviceIdAsync(Guid deviceId);
        public Task<IEnumerable<T>> GetBySensorIdAsync(Guid sensorGuid);
        public Task<IEnumerable<T>> GetBySensorIDAndDateAsync(Guid sensorGuid, DateTime StartDateTime, DateTime StopDateTime);

        public Task<T> CreateAsync(T item);

        public Task<T> UpdateAsync(T item);

        public Task<T> DeleteAsync(T item);


    }
}
