﻿using GardenControl.API.Contexts;
using GardenControl.API.Interfaces;
using GardenControl.Core.Entitys;

using Microsoft.EntityFrameworkCore;

namespace GardenControl.API.Repositorys
{
    public class EfRepositoryTemperature<T> : IRepository<T> where T : Temperature
    {
        private readonly DataContext _dataContext;
        public EfRepositoryTemperature(DataContext dataContext)
        {
            _dataContext = dataContext;
        }


        private IQueryable<T> GetAllQueryable()
        {
            return _dataContext.Set<T>().AsQueryable();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var res = await GetAllQueryable().ToListAsync();
            return res;
        }


        public async Task<T> GetByIdAsync(long id)
        {
            var res = await GetAllQueryable().FirstOrDefaultAsync(x => x.Id == id);
            return res;
        }

        public async Task<IEnumerable<T>> GetByDeviceIdAsync(Guid deviceId)
        {
            var res = await GetAllQueryable().Where(x => x.DeviceId.Equals(deviceId)).ToListAsync();

            return res;

        }

        public async Task<IEnumerable<T>> GetBySensorIdAsync(Guid sensorGuid)
        {
            var res = await GetAllQueryable().Where(x => x.SensorGuid.Equals(sensorGuid)).ToListAsync();

            return res;
        }

        public async Task<IEnumerable<T>> GetBySensorIDAndDateAsync(Guid sensorGuid, DateTime StartDateTime, DateTime StopDateTime)
        {
            var res = await GetAllQueryable().Where(x => x.DateTime >= StartDateTime && x.DateTime <= StopDateTime)
                .ToListAsync();

            return res;
        }

        public async Task<T> CreateAsync(T item)
        {
            var res = await _dataContext.Set<T>().AddAsync(item);
            await _dataContext.SaveChangesAsync();
            return res.Entity;

        }

        public async Task<T> UpdateAsync(T entity)
        {
            _dataContext.Set<T>().Update(entity);
            await _dataContext.SaveChangesAsync();
            return entity;
        }

        public Task<T> DeleteAsync(T item)
        {
            _dataContext.Set<T>().Remove(item);
            _dataContext.SaveChanges();
            return Task.FromResult(item);
        }

    }
}
