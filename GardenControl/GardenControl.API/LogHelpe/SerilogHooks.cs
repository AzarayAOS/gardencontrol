﻿using Serilog.Sinks.File.Archive;
using Serilog.Sinks.File.Header;

using System.IO.Compression;

namespace GardenControl.API.LogHelpe
{
    public class SerilogHooks
    {
        public static HeaderWriter MyHeaderWriter => new HeaderWriter("Timestamp,Level,Source Context,Message");
        public static ArchiveHooks MyArchiveHooks => new ArchiveHooks(CompressionLevel.Optimal, @"\PathLog\");
    }
}
